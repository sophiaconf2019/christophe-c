FROM gcc as build
WORKDIR /tiny
COPY Tiny-WebServer ./
RUN make clean
RUN make

FROM fedora
COPY --from=build tiny/tiny tiny/home.html tiny/faye.jpg ./
CMD ["./tiny"]
EXPOSE 8000
